from django.forms import model_to_dict
from django.http import JsonResponse
from django.views import View
from django.views.generic import TemplateView
from personnel.models import PersonnelModel


class Index(TemplateView):
    template_name = 'personnel/index.html'


class Personnel(View):
    def get(self, request):
        personnel = [model_to_dict(x) for x in PersonnelModel.objects.all()]

        return JsonResponse({'data': personnel})

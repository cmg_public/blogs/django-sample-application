from django.db import models


class PersonnelModel(models.Model):
    name = models.CharField(db_column='Name', max_length=256)
    phone = models.CharField(db_column='Phone', max_length=32)
    email = models.EmailField(db_column='Email', max_length=64)
    address = models.CharField(db_column='Address', max_length=128)
    zip = models.IntegerField(db_column='Zip')
    country = models.CharField(db_column='Country', max_length=128)
    salary = models.IntegerField(db_column='Salary')

from django.urls import path

from . import views

urlpatterns = [
    path('personnel', views.Index.as_view(), name='personnel-index'),
    path('personnel/data', views.Personnel.as_view(), name='personnel-data'),
]
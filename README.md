# Django Sample Application

# Purpose

This repository is used to provide an introduction to Django and the components that can be built on top of it to improve the functionality and usability of the application.

# Setup

This repository requires very little setup to get working. There is a provided requirements.txt file that can be used to setup a virtual environment for running the application. Additionally, there is an included SQL Lite database that already has the needed data to run the application. There are different branches included in this repository that build on top of one another to see the progression of the application buildout.

# Blogs

There are blogs that have been written to go along with this repository, which can be found below:
- [API Development in Django: Accessing Your Data With REST](https://www.515tech.com/post/api-development-in-django)
- [Swagger For Django: Creating Useable API Documentation](https://www.515tech.com/post/swagger-for-django-creating-useable-api-documentation)
- [LDAP and Django: Securing Your Application Within a Windows Domain](https://www.515tech.com/post/ldap-and-django-securing-your-application-within-a-windows-domain)